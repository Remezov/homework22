package ru.remezov.comparator;

import java.util.Comparator;

public class PersonSuperComparator implements Comparator<Person> {


    @Override
    public int compare(Person o1, Person o2) {
        int compareName = o1.getName().compareTo(o2.getName());
        int compareAge = o1.getAge().compareTo(o2.getAge());

        if(compareName == 0) {
            return compareAge;
        }
        else {
            return compareName;
        }
    }
}
