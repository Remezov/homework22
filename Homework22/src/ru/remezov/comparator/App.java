package ru.remezov.comparator;

import java.util.*;

public class App {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Вася",45));
        persons.add(new Person("Миша",30));
        persons.add(new Person("Петя",18));
        persons.add(new Person("Вася",18));
        Collections.sort(persons, new PersonSuperComparator());

        for (Person person: persons) {
            System.out.println(person);
        }
    }
}
